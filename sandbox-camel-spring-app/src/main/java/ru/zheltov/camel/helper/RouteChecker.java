package ru.zheltov.camel.helper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Route;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
@RequiredArgsConstructor
@Slf4j
public class RouteChecker {

    private final CamelContext camelContext;

    /**
     * Эта информация уже есть в логах. Она нужна лишь для демонстрации возможности доступа к CamelContext
     */
    @EventListener(ApplicationStartedEvent.class)
    public void checkRoutes() {
        camelContext.getRoutes().stream()
                .sorted(Comparator.comparing(Route::getStartupOrder))
                .forEach(route -> log.info("Route @ endpoint {} has id {}", route.getEndpoint(), route.getId()));
    }
}
