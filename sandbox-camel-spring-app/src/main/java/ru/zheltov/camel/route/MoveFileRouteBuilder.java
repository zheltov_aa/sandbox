package ru.zheltov.camel.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MoveFileRouteBuilder extends RouteBuilder {

    @Value("${routes.file.input}")
    private String inputDir;

    @Value("${routes.file.output}")
    private String outputDir;

    //TODO сделать переменные для входного пути
    @Override
    public void configure() {
        from("file:" + inputDir)
                .log("Received file: ${header.CamelFileName}")
                .to("file:" + outputDir);
    }
}
