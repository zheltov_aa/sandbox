package ru.zheltov.spring.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "service", ignoreUnknownFields = false)
@Data
public class YamlNodeToProperties {

    private final FeatureEnabled featureEnabled;
}
