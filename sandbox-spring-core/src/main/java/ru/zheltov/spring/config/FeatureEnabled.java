package ru.zheltov.spring.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class FeatureEnabled {

    private Boolean feature1;
    private Feature2 feature2 = new Feature2();

    @Data
    public static class Feature2 {
        private Boolean field;
    }
}
