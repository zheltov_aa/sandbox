package ru.zheltov.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.UUID;

@Slf4j
@Configuration
@EnableScheduling
public class AppConfig {

    public static final String INSTANCE_ID = UUID.randomUUID().toString();
}
