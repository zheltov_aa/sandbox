package ru.zheltov.spring.beans;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@NoArgsConstructor
@Slf4j
public class ConditionalComponent {

    /**
     * Бин должен подниматься, если в yml присутствует нужное свойство.
     * Значение - не имеет значение. Просто есть или нет
     */
    @ConditionalOnProperty("beanCreationCondition")
    @Bean(name = "beanCreationCondition")
    public ConditionalComponentExactCase property() {
        return new ConditionalComponentExactCase("ConditionalOnProperty");
    }

    /**
     * Бин должен подниматься, если в yml присутствует нужное свойство с проверкой значения.
     * Значение проверяет через SpEL-механизм Spring
     */
    @ConditionalOnExpression("'${beanCreationExpression}'.equals('123')")
    @Bean(name = "beanCreationExpression")
    public ConditionalComponentExactCase expression() {
        return new ConditionalComponentExactCase("ConditionalOnExpression");
    }


    public static class ConditionalComponentExactCase {

        public ConditionalComponentExactCase(String mark) {
            log.info(">>> {} bean created", mark);
        }
    }
}
