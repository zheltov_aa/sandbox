package ru.zheltov.spring.beans;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Интересный кейс, как сделать надежный парсинг в @Value с защитой от опечаток в значении (НО НЕ в имени параметра)
 * Интересно, что нельзя использовать @AllArgsConstructor совместно с @Value
 */
@Component
@Slf4j
public class FeatureToggling {

    private final Boolean featureEnabled;

    public FeatureToggling(@Value("#{new Boolean('${someFeature.enabled}')}") Boolean featureEnabled) {
        this.featureEnabled = featureEnabled;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        log.info("featureEnabled: {}", this.featureEnabled);
    }
}
