package ru.zheltov.camel.lib;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String filePath = "names.xlsx"; // Файл находится в корне resources
        XlsxToJsonProcessor processor = new XlsxToJsonProcessor();

        try (InputStream inputStream = Main.class.getClassLoader().getResourceAsStream(filePath)) {
            if (inputStream == null) {
                System.err.println("Файл не найден: " + filePath);
                return;
            }

            List<String> jsonList = processor.processXlsxToJson(inputStream);
            for (String json : jsonList) {
                System.out.println(json);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
