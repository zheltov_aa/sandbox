package ru.zheltov.sandbox.quiz.stream;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class FlatMap {

    @Test
    public void findMaxLength() {
        List<List<String>> arg = Arrays.asList(getStringList(3), getStringList(2));
        Optional<String> result = arg.stream()
                .flatMap(Collection::stream)
                .max(Comparator.comparing(String::length));
        Assert.assertTrue(result.isPresent());
    }

    private List<String> getStringList(int count) {
        List<String> result = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            String item = String.valueOf(i).repeat(i);
            result.add(item);
        }
        return result;
    }
}
