package ru.zheltov.sandbox.quiz.misc;

import org.junit.Assert;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс демонстрирует получение значения enum через однократно инициализируемую мапу
 */
public enum EnumValues {

    FIRST("code1", "value1"),
    SECOND("code2", "value2"),
    THIRD("code3", "value3");

    private final String code;
    private final String value;

    private static final Map<String, EnumValues> valueByCode = new HashMap<>();

    static {
        Arrays.stream(EnumValues.values())
                .forEach(item -> valueByCode.put(item.code, item));
    }

    EnumValues(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private static EnumValues getValueByCode(String code) {
        return valueByCode.get(code);
    }

    public static void main(String[] args) {
        Assert.assertEquals(getValueByCode("code1"), FIRST);
        Assert.assertNull(getValueByCode("code_unknown"));
    }
}
