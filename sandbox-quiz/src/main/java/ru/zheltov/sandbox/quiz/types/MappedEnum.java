package ru.zheltov.sandbox.quiz.types;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс демонстрирует ???
 */
public enum MappedEnum {

    A("A", 1),
    B("B", 2),
    C("C", 3);

    @Getter
    private String letter;

    @Getter
    private int digit;

    MappedEnum(String letter, int digit) {
        this.letter = letter;
        this.digit = digit;
        MapHolder.mapper.put(letter, this);
    }

    public static MappedEnum getByKey(String key) {
        return MapHolder.mapper.get(key);
    }

    public static void main(String[] args) {
        System.out.println(A.name());
    }

    ////////////////////

    private static class MapHolder {
        private static Map<String, MappedEnum> mapper = new HashMap<>();
    }
}
