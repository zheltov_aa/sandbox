package ru.zheltov.sandbox.quiz.collection;

import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MapTest {

    @Test(expected = NullPointerException.class)
    public void mapRemove() {
        Map<String, String> map = new ConcurrentHashMap<>();
        map.remove(null);
    }
}
