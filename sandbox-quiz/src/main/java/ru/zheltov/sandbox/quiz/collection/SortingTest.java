package ru.zheltov.sandbox.quiz.collection;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SortingTest {

    /**
     * Пример с сортировкой коллекции через Comparator
     */
    //TODO переделать на asserts
    @Test
    public void withComparator() {
        List<String> src = Arrays.asList("a", "c", null, "b", "1");
        src.sort((o1, o2) -> {
            if ((o1 == null) && (o2 == null)) {
                return 0;
            } else if (o1 == null) {
                return -1;
            } else if (o2 == null) {
                return 1;
            } else {
                return o1.compareTo(o2);
            }
        });
        //TODO переделать на Assert'ы
        System.out.println(src);
    }
}
