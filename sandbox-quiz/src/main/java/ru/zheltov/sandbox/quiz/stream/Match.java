package ru.zheltov.sandbox.quiz.stream;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Класс демонстрирует работу поиска совпадения:
 *   * поиск на отсутствие совпадений
 *   * поиск на первое совпадение
 * Особенность noneMatch и anyMatch - поиск прерывается на первом совпадении
 */
public class Match {

    @Test
    public void noneMatch() {
        boolean result = IntStream.range(0, 10)
                .noneMatch(i -> i == 5);
        Assert.assertFalse(result);
    }

    @Test
    public void anyMatch() {
        boolean result = IntStream.range(0, 10)
                .anyMatch(i -> i == 5);
        Assert.assertTrue(result);
    }

    @Test
    public void findAny() {
        final String sample = "MATCH!";
        Optional<String> any = Stream.of("1", "2", "3", sample, "4", "5", sample)
                .filter(sample::equals)
                .findAny();
        Assert.assertTrue(any.isPresent() && any.get().equals(sample));
    }
}
