package ru.zheltov.sandbox.quiz.operations;

/**
 * Класс демонстрирует ???
 */
public class Equal {

    public static void main(String[] args) {
        strings();
        booleans();
        integers();
    }

    private static void strings() {
        String a, b;

        a = new String("abc");
        b = new String("abc");
        System.out.println("Strings via NEW: " + (a == b));

        a = "abc";
        b = "abc";
        System.out.println("Strings via EXACT: " + (a == b));
    }

    public static void booleans() {
        boolean a, b;

        a = new Boolean(true);
        b = new Boolean(true);
        System.out.println("Boolean via NEW: " + (a == b));

        a = Boolean.TRUE;
        b = Boolean.TRUE;
        System.out.println("Boolean via Boolean.TRUE: " + (a == b));

        a = true;
        b = new Boolean(true);
        System.out.println("Boolean via MIXED (exact + new): " + (a == b));
    }

    public static void integers() {
        Integer a, b;

        a = new Integer(100);
        b = new Integer(100);
        System.out.println("Integers via NEW (<127): " + (a == b));

        a = 100;
        b = 100;
        System.out.println("Integers via EXACT (<127): " + (a == b));

        a = 1000;
        b = 1000;
        System.out.println("Integers via EXACT (>127): " + (a == b));
    }
}
