package ru.zheltov.sandbox.quiz.init;

import org.junit.Assert;
import org.junit.Test;

import lombok.Getter;
import lombok.Setter;

public class NonFinalCallTest {

    /**
     * Тест демонстрирует почему нельзя в конструктор не-финальные методы помещать
     */
    @Test
    public void overrideConstructor() {
        B b = new B();
        Assert.assertEquals(2, b.getValue());
    }

    @Setter
    @Getter
    public static class A {

        private int value;

        public A() {
            init();
        }

        public void init() {
            setValue(1);
        }
    }

    public static class B extends A {

        public B() {
            super();
        }

        @Override
        public void init() {
            setValue(2);
        }
    }
}
