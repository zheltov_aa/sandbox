package ru.zheltov.sandbox.quiz.stream;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class Filter {

    /**
     * Демонстрирует неизменность исходной коллекции при применении фильтра к ней
     */
    @Test
    public void stream() {
        List<String> base = Arrays.asList("A", "B", "C", "D");
        List<String> other = Arrays.asList("A", "B");
        //Будет создан новый лист, а base останется нетронутым
        List<String> result = base.stream()
                .filter(str ->  !other.contains(str))
                .toList();
        Assert.assertEquals(4, base.size());
        Assert.assertEquals(2, other.size());
        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains("C"));
        Assert.assertTrue(result.contains("D"));
    }
}
