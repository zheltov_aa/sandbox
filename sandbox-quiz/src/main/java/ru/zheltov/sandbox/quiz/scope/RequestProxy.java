package ru.zheltov.sandbox.quiz.scope;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

/**
 * Класс демонстрирует поведение Spring'а при обращении с бинами реквестового скоупа
 */
@Slf4j
@SpringBootApplication
@ComponentScan("ru.zheltov.sandbox.quiz.scope")
@EnableScheduling
public class RequestProxy {

    public static final String INSTANCE_ID = UUID.randomUUID().toString();

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = SpringApplication.run(RequestProxy.class, args);
        log.info("Instance ID: {}", INSTANCE_ID);
        TimeUnit.SECONDS.sleep(1);
        //Шлем пачку запросов
        int count = 3;
        log.info("Sending {} requests...", count);
        for (int i = 0; i < count; i++) {
            sendRequest(i);
        }
        //Делаем вызов метода не из контекста реквеста
        TimeUnit.SECONDS.sleep(3);
        MyService service = context.getBean(MyService.class);
        service.printMap("NOT FROM REQUEST");
    }

    private static void sendRequest(int value) {
        Thread th = new Thread(() -> {
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080//scope/" + value;
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            log.info("{} request return status {}", value, response.getStatusCode());
        });
        th.start();
    }

    //////////////


    @RestController
    @AllArgsConstructor
    private class Controller {

        private MyRequest request;
        private MyService service;

        @GetMapping(value = "/scope/{param}")
        public int testSmth(@PathVariable("param") String param) throws InterruptedException {
            log.info("testSmth starts with {}", param);
            TimeUnit.SECONDS.sleep(1);
            request.setParam(param);
            service.addRequest(request);
            return service.getSize();
        }
    }


    @Service
    private class MyService {

        private Map<String, MyRequest> requestMap = new HashMap<>();

        void addRequest(MyRequest request) {
            requestMap.putIfAbsent(request.getParam(), request);
            log.info("New entry {} added. Current size is {}", request, requestMap.keySet().size());
            printMap("Printing from addRequest");
        }

        public int getSize() {
            return requestMap.keySet().size();
        }

        public void printMap(String msg) {
            StringBuilder sb = new StringBuilder(">>> " + msg + "\n");
            try {
                Set<Map.Entry> allEntries = requestMap.entrySet().stream()
                        .peek(entry -> {
                            sb.append("\tKey = " + entry.getKey() + ", Value = " + String.valueOf(entry.getValue()) + "\n");
                        })
                        .collect(Collectors.toSet());
                sb.append("\tTotal entries count: " + allEntries.size());
            } catch (Throwable th) {
                sb.append("ERROR: Printing failed\nType: " + th.getClass() + "\nMessage: " + th.getMessage());
            } finally {
                log.info(sb.toString());
            }
        }
    }


    @Component
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
    @NoArgsConstructor
    @Setter
    @Getter
    private class MyRequest {

        private String param;
    }
}
