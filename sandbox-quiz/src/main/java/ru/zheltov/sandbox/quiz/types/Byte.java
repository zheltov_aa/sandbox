package ru.zheltov.sandbox.quiz.types;

/**
 * Класс демонстрирует ???
 */
public class Byte {

    public static void main(String[] args) {
//        casting();
//        getByte();
        CBA cba = null;
        switch (cba) {
            case A:
                System.out.println("A");
                break;
            default:
                System.out.println("def: " + cba);
                break;
        }
        System.out.println("the end");
    }

    private static void getByte() {
        Integer i = new Integer(1234);
        System.out.println(i.byteValue());
        i += 256;
        System.out.println(i.byteValue());
    }

    private static void casting() {
        Object i = new Integer(12345);
        System.out.println("Int " + i);
        System.out.println("byte " + ((byte)i));
    }

    ////////////////////

    protected enum CBA {
        A,
        B,
        C;
    }
}
