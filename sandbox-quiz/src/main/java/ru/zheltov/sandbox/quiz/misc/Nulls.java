package ru.zheltov.sandbox.quiz.misc;

import jakarta.annotation.Nonnull;
import org.junit.Test;
import org.springframework.lang.Nullable;

/**
 * Демонстрирует "раскраску" кода IDEA при наличии разметки NotNull и Nullable
 */
public class Nulls {

    @Test
    public void check() {
        System.out.println(getWithNull("WithNull", null));
        System.out.println(getWithoutNull("WithoutNull", "non null!"));
    }

    @Test(expected = NullPointerException.class)
    public void checkFail() {
        System.out.println(getWithNullMarkup("WithNull", null));
    }

    private @Nonnull String getWithNull(String msg, @Nullable String nullable) {
        if (nullable == null) {
            System.out.println("Null value is legal in this case");
            return msg.toUpperCase();
        } else {
            return msg.toUpperCase() + " >> " + nullable.toUpperCase();
        }
    }

    private @Nonnull String getWithNullMarkup(String msg, @Nullable String nullable) {
        return msg.toUpperCase() + " >> " + nullable.toUpperCase();
    }

    private @Nonnull String getWithoutNull(String msg, @Nonnull String nonNull) {
        return msg.toUpperCase() + " >> " + nonNull.toUpperCase();
    }
}
