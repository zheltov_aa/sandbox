package ru.zheltov.sandbox.quiz.stream;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Класс демонстрирует разные варианты поиска значения в коллекции
 */
public class FindFirst {

    @Test
    public void findViaStream() {
        String str = Stream.of(1, 2, 3, 4, 5)
                .map(String::valueOf)
                .filter("3"::equals)
                .findFirst()
                .orElse("null");
        Assert.assertEquals(str, "3");
    }
}
