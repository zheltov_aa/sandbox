package ru.zheltov.sandbox.quiz.collection;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImmutableTest {

    /**
     * Демонстрирует возможность модификации иммутабельной коллекции
     */
    @Test
    public void addToImmutableIsPosible() {
        List<String> baseList = new ArrayList<>();
        baseList.add("1");
        baseList.add("2");
        List<String> immutable = Collections.unmodifiableList(baseList);
        Assert.assertEquals(immutable.size(), 2);

        baseList.add("3");
        Assert.assertNotEquals(immutable.size(), 2);
    }
}
