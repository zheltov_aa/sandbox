package ru.zheltov.sandbox.quiz.oop;

/**
 * Класс демонстрирует ???
 */
public class OverrideCase {

    public static final void main(String[] arg) {
        B b = new B();
        System.out.println(b.getI());
    }

    ////////////////////

    static class A {

        int i = 1;

        public A() {
            setI();
        }

        public void setI() {
            i = 2;
        }
    }

    ////////////////////

    static class B extends A {

        @Override
        public void setI() {
            i = 3;
        }

        public int getI() {
            return i;
        }
    }
}
