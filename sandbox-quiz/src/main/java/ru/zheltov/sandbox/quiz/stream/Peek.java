package ru.zheltov.sandbox.quiz.stream;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс демонстрирует работу peek в стриме
 */
public class Peek {

    public static void main(String[] args) {
        Stream.of("one", "two", "three", "four")
                .peek(e -> System.out.println("Peek before filter with value: " + e))
                .filter(e -> e.length() > 3)
                .map(String::toUpperCase)
                .peek(e -> System.out.println("Mapped value: " + e))
                .collect(Collectors.toList());
    }
}
