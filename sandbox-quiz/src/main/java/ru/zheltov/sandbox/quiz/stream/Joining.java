package ru.zheltov.sandbox.quiz.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс демонстрирует ???
 */
public class Joining {

    public static void main(String[] args) {
//        asStream();
        asList();
    }

    private static void asStream() {

        String str = Stream.of("surename", "name", "patronymic")
                .collect(Collectors.joining(" "));
        System.out.println(str);
    }

    private static void asList() {
        String list = Stream.of("b", "a", "c")
                .sorted()
                .collect(Collectors.joining(System.lineSeparator()));
        System.out.println(list);
        System.out.println("-=end=-");
    }
}
