package ru.zheltov.sandbox.quiz.lombok;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.With;

/**
 * Класс демонстрирует возможность быстрого изготовления клона с минимумом букв
 */
public class WitherClone {

    public static void main(String[] args) {
        System.out.println("Разные экземпляры:");
        A origin = A.builder()
                .age(18)
                .name("Вася")
                .build();
        A copy = origin.withName("Коля");
        copy.setAge(30);
        System.out.println(origin);
        System.out.println(copy);

        System.out.println("Одинаковые экземпляры:");
        A copy2 = origin.withName(origin.getName());
        copy2.setAge(25);
        System.out.println(origin);
        System.out.println(copy2);
        System.out.println(copy);
    }

    @Builder
    @AllArgsConstructor
    @ToString
    private static class A {

        @Setter(value = AccessLevel.PRIVATE)
        private int age;

        @With(value = AccessLevel.PRIVATE)
        @Getter(value = AccessLevel.PRIVATE)
        private String name;
    }
}
