package ru.zheltov.sandbox.quiz.misc;

/**
 * Класс демонстрирует необходимость и правильный вариант отработки при конфликте имплементируемых интерфейсов с default
 */
public class MultiChoise {

    public static void main(String[] args) {

        MyTestClazz myClazz = new MyTestClazz();
        myClazz.doSmth();
    }

    ////////////////////

    public interface MySimpleInterface {

        default void doSmth() {
            System.out.println("MySimpleInterface");
        }

        void doSmth1();
    }

    ////////////////////

    public interface MyComplexInterface {

        default void doSmth() {
            System.out.println("MyComplexInterface");
        }

        default void doSmthElse() {
            System.out.println("MyComplexInterface - else");
        }

        void doSmth1();
    }

    ////////////////////

    public static class MyTestClazz implements MySimpleInterface, MyComplexInterface {

        @Override
        public void doSmth() {
            MySimpleInterface.super.doSmth();
        }

        @Override
        public void doSmth1() {
            //
        }
    }
}
