package ru.zheltov.sandbox.quiz.collection;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class SetTest {

    @Test
    public void addItemToHashSet() {
        Set<String> set = new HashSet<>();
        Assert.assertTrue(set.add("1"));
        Assert.assertTrue(set.add("2"));
        Assert.assertFalse(set.add("1"));
        Assert.assertEquals(set.size(), 2);
    }
}
