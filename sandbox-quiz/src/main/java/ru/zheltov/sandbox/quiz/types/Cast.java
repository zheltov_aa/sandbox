package ru.zheltov.sandbox.quiz.types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Класс демонстрирует ???
 */
public class Cast {

    public static void main(String[] args) throws ParseException {
//        wrongCasting();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
        Date d = sdf.parse("2018-08-22 00:00:00.000");
        System.out.println(d);
    }

    private static void wrongCasting() {
        Object base = new ArrayList<HashMap<String, Integer>>();
        Map<String, Integer> testMap = new HashMap();
        testMap.put("key", 1234);
        List<String> result = (List<String>) base;
        System.out.println(result);
    }
}
