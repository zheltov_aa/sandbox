package ru.zheltov.sandbox.quiz.stream;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapFilling {

    @Test
    public void fill() {
        Map<String, List<String>> result = new HashMap<>();
        for(Data item : Data.values()) {
            result.computeIfAbsent(item.key, value -> new ArrayList<>()).add(item.value);
        }
        System.out.println(result);
    }

    private enum Data {

        A("1", "1A"),
        B("2", "2A"),
        C("3", "3A"),
        D("1", "1B");

        private final String key;
        private final String value;

        Data(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
