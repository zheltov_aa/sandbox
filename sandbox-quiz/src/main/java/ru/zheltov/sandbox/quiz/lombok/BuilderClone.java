package ru.zheltov.sandbox.quiz.lombok;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

@Slf4j
public class BuilderClone {

    @Test
    public void fullCloneWithBuilder() {
        Entry entry = new Entry("SomeName", 42L);
        Entry copy = entry.toBuilder().build();
        Assert.assertEquals(entry, copy);
        Assert.assertNotSame(entry, copy);
    }

    @Data
    @Builder(toBuilder = true)
    private static class Entry {
        private String name;
        private Object value;
    }
}
