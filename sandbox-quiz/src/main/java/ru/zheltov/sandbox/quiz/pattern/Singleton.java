package ru.zheltov.sandbox.quiz.pattern;

/**
 * Класс демонстрирует паттерн Singleton с ленивой инициализацией
 */
public class Singleton {

    static {
        System.out.println("initializing [Singleton]");
    }

    private Singleton() {
        //
    }

    public static Singleton getInstance() {
        return InstanceHandler.instance;
    }

    public static void main(String[] args) {
        System.out.println("main method started");
        System.out.println(getInstance());
    }

    ////////////////////

    private static class InstanceHandler {

        private static Singleton instance = new Singleton();

        static {
            System.out.println("initializing [InstanceHandler]");
        }
    }
}
