package ru.zheltov.sandbox.quiz.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class ParamsResolving {

    @A
    @B(type = ParamsResolving.class)  //should be resolved, as @B has not "value" method
    @C(ParamsResolving.class)  //by default use a "value" method (if exists, or design-time error if not)
    public static void main(String[] args) {
        //Nothing to do
    }

    @Target(value= ElementType.METHOD)
    @Retention(value= RetentionPolicy.RUNTIME)
    public @interface A {
        Class type() default ParamsResolving.class;
    }

    @Target(value= ElementType.METHOD)
    @Retention(value= RetentionPolicy.RUNTIME)
    public @interface B {
        Class type() default ParamsResolving.class;
        Class otherType() default ParamsResolving.class;
    }

    @Target(value= ElementType.METHOD)
    @Retention(value= RetentionPolicy.RUNTIME)
    public @interface C {
        Class type() default ParamsResolving.class;
        Class value() default ParamsResolving.class;
    }
}
