package ru.zheltov.sandbox.quiz.misc;

/**
 * Класс демонстрирует StackOverflow
 */
public class Infinity {

    public static void main(String[] args) {
        System.out.println("Hello!");
        try {
            doSmth();
        } catch(StackOverflowError sof) {
            System.out.println(sof);
        }
        System.out.println("Bye!");
    }

    private static void doSmth() {
        while(true) {
            doSmth();
        }
    }
}
