package ru.zheltov.sandbox.quiz.types;

import org.junit.Test;

import org.apache.commons.validator.routines.UrlValidator;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

public class Url {

    public static final String[] SCHEMES = {"http","https"};

    @Test
    public void normalize() throws URISyntaxException, MalformedURLException {
        URI initialValue = new URI("https://www.baeldung.com//hibernate-5-spring?ip=123");
        UrlValidator urlValidator = new UrlValidator(SCHEMES);
        URI normalized = initialValue.normalize();
        if (urlValidator.isValid(normalized.toString())) {
            System.out.println("Normalized: " + normalized.toURL());
            String host = normalized.toURL().getHost();
            System.out.println("Valid: " + host);
            System.out.println("Hash: " + host.hashCode());
        } else {
            System.out.println("NOT Valid: " + normalized);
        }
    }
}
