package ru.zheltov.sandbox.quiz.chars;

import org.junit.Assert;
import org.junit.Test;

import java.util.stream.Stream;

public class StringTest {

    /**
     * Демонстрирует некорректную работу с границами подстроки
     */
    @Test(expected = StringIndexOutOfBoundsException.class)
    public void range() {
        String base = "12345";
        base.substring(0, base.indexOf(';'));
    }

    /**
     * Демонстрирует работу split
     */
    @Test
    public void split() {
        String base = "Фамилия  Имя    Отчество ВотороеОтчетсво";
        String[] result = base.split(" ", 3);
        Assert.assertEquals(result.length, 3);
        printValues(result);

        String cleared = base.trim().replaceAll("\\s{2,}", " ");
        result = cleared.split(" ", 3);
        printValues(result);
    }

    private void printValues(String[] result) {
        Stream.of(result)
                .forEach(item -> System.out.println(String.format("[%02d] <%s>", item.length(), item)));
    }

    @Test
    public void newString() {
        Assert.assertNotSame(new String("hello"), new String("hello"));
        Assert.assertSame("hello", "hello");
    }
}
