package ru.zheltov.sandbox.quiz.misc;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class Randomizer {

    @Test
    public void random() {
        Random randomizer = new Random();
        long long1 = randomizer.nextLong();
        long long2 = randomizer.nextLong();
        Assert.assertNotEquals(long1, long2);
    }
}
