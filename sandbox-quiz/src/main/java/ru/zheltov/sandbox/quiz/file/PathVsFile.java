package ru.zheltov.sandbox.quiz.file;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Класс демонстрирует ???
 */
public class PathVsFile {

    public static void main(String[] args) {
        String dir = "c:/";
        String file = "..\\1.txt";
        System.out.println(FilenameUtils.getFullPath("d:/123/dir1") + File.separator);
        try {
            System.out.println("path: " + getViaPath(dir,file));
            System.out.println("file: " + getViaFile(dir,file));
        } catch (Exception ex) {
            System.out.println("exception: " + ex);
        }
    }

    private static String getViaPath(String dir, String name) {
        return Paths.get(dir, FilenameUtils.getName(name))
                .normalize().toString();
    }

    private static String getViaFile(String dir, String name) throws IOException {
        File file = new File(dir, FilenameUtils.getName(name));
        return file.getCanonicalPath();
    }
}
