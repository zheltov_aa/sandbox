package ru.zheltov.sandbox.quiz.types;

import java.math.BigDecimal;

/**
 * Класс демонстрирует ???
 */
public class Character {

    public static void main(String[] args) {
        java.lang.Character ch;

        ch = '1';
        System.out.println(ch == '1');
        System.out.println(ch == '2');

        BigDecimal bd = new BigDecimal(new Float(0));
        System.out.println("default: " + bd.toString());
        System.out.println("toPlaintString: " + bigDecimalTostring(bd));
    }

    private static String bigDecimalTostring(BigDecimal value) {
        if (value != null) {
            return value.toPlainString();
        } else {
            return null;
        }
    }
}
