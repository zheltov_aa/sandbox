package ru.zheltov.sandbox.quiz.exception;

import org.junit.Test;

/**
 * Демонстрирует поведение конструкции try-with-resources при различных сочетаниях
 */
public class ExcBehave {

    @Test
    public void allOk() throws Exception {
        //TODO переделать на ассерты
        System.out.println(doOk());
    }

    private Boolean doOk() throws Exception {
        try(MyGoodResource mr = new MyGoodResource()) {
            System.out.println(">>> try");
            return null;
        } catch(Exception ex) {
            printException(ex);
            return false;
        } finally {
            System.out.println(">>> finally");
            return true; //Обрати внимание - IDE намекает на некорректность ретурна из finally
        }
    }

    @Test
    public void failedTry() throws Exception {
        try(MyGoodResource mr = new MyGoodResource()) {
            System.out.println(">>> try");
            throw new Exception("On-Try Exception");
        } catch(Exception ex) {
            printException(ex);
        } finally {
            System.out.println(">>> finally");
        }
    }

    @Test
    public void failedTryAndResource() throws Exception {
        try(MyResource mr = new MyBadResource()) {
            System.out.println(">>> try");
            throw new Exception("On-Try Exception");
        } catch(Exception ex) {
            printException(ex);
        } finally {
            System.out.println(">>> finally");
        }
    }

    @Test(expected = OutgoingException.class)
    public void failedTryCatchAndResource() throws Exception {
        try(MyResource mr = new MyBadResource()) {
            System.out.println(">>> try");
            throw new Exception("On-Try Exception");
        } catch(Exception ex) {
            printException(ex);
            throw new OutgoingException("On-Catch Exception");
        } finally {
            System.out.println(">>> finally");
        }
    }

    private void printException(Exception ex) {
        System.out.println(">>> catch");
        System.out.println("    --> FULL StackTrace: ");
        ex.printStackTrace();
        System.out.println("    <-- end of StackTrace\n");
    }

    ///////////

    interface MyResource extends AutoCloseable {}

    static class MyGoodResource implements MyResource {
        @Override
        public void close() throws Exception {
            System.out.println(">>> AutoCloseable");
        }
    }

    static class MyBadResource implements MyResource {
        @Override
        public void close() throws Exception {
            throw new Exception("On-Resource-Close Exception");
        }
    }

    static class OutgoingException extends Exception {
        OutgoingException(String msg) {
            super(msg);
        }
    }
}
